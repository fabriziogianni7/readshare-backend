/**
 * 
 */
package it.fabri.code.readshare.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fabri.code.readshare.entities.BookEntity;

/**
 * @author Bringhi7
 */
@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {
	
	BookEntity findByTitle(String title);
	BookEntity findByAuthor(String author);
	BookEntity findByGenre(String genre);
	BookEntity findByYear(String year);
	
	

}
