package it.fabri.code.readshare.utils;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class NicknameGenerator {
	
	String[] animals = {
			"Dog", 
			"Cat",
			"Mouse",
			"Lion",
			"Lizard",
			"Pigeon",
			"Ant",
			"Eagle",
			"Bull",
			"Rat",
			"Unicorn",
			"Dinosaur",
			"Dragon",
			"Chicken",
			"Calf",
			"Pup",
			"Whelp",
			"Kitten",
			"Puma",
			"Pig",
			"Albatros",
			"Alligator",
			"Antelope",
			"Ape",
			"Barracuda",
			"Stallion",
			"Bee",
			"Crab",
			"Cobra",
			"Dogfish",
			"Flamingo",
			"Fox",
			"Frog",
			"Gecko",
			"Panda",
			"Gnu",
			"Goat",
			"Gorilla",
			"Hamster",
			"Horse",
			"Iguana",
			"Jackal",
			"Kangarooo",
			"Lemur",
			"Leopard",
			"Mandrill",
			"Marmot",
			"Octopus",
			"Panther",
			"Penguin",
			"Polarbear",
			"Quetzal",
			"Rabbit",
			"Raccoon",
			"Ram",
			"Red panda",
			"Elephant "};
	String[] adjective = {
			"Adorable", 
			"Adventurous", 
			"Aggressive", 
			"Angry", 
			"Annoying", 
			"Arrogant", 
			"Hungry", 
			"Disturbed", 
			"Scary", 
			"Shiny", 
			"Silly", 
			"Itchy", 
			"Sleepy", 
			"Easy", 
			"Sparkling", 
			"Lazy", 
			"Strange", 
			"Horny", 
			"Super", 
			"Lucky", 
			"Exuberant", 
			"Tasty", 
			"Nasty", 
			"Mysterious", 
			"Colorful", 
			"Old-fashioned", 
			"Confused", 
			"Outstanding", 
			"Creepy", 
			"Handsome", 
			"cute", 
			"Powerful", 
			"Proud", 
			"Dark", 
			"Proud", 
			"Wild", 
			"Wide-eyed", 
			"Gorgeous", 
			"Courageous", 
			"Good", 
			"Nutty", 
			"Cheerful", 
			"Foolish", 
			"Rich"};
	String[] verbs = {
			"Amusing",
			"Burning",
			"Buzzing",
			"Beaming",
			"Claiming",
			"Arguing",
			"Crying",
			"Cheering",
			"Complaining",
			"Coughing",
			"Clapping",
			"Disapproving",
			"Dancing",
			"Exploding",
			"Hammering",
			"Jamming",
			"Joking",
			"Loving",
			"Rocking",
			"Relaxing",
			"Smoking",
			"Tumbling",
			"Laughing",
			"Tickling",
			"Training",
			"Stunting",
			"Taunting",
					"Applauding"};
	
	public int generateNumber(int max) { 
		Random rn = new Random();
		return (rn.nextInt(max));
	}
	
	public String generateNickname() {
		return (verbs[generateNumber(verbs.length)]+" "+adjective[generateNumber(adjective.length)]+" "+animals[generateNumber(animals.length)]);
	}
	
//	public static void main (String[] args) {
//		NicknameGenerator gen = new NicknameGenerator();
//		System.out.println(gen.generaFrase());
//	}

}
