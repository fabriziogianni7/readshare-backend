/**
 * 
 */
package it.fabri.code.readshare.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.fabri.code.readshare.entities.BookEntity;
import it.fabri.code.readshare.repositories.BookRepository;
import it.fabri.code.readshare.services.BookService;

/**
 * @author Bringhi7
 */
@EnableAutoConfiguration
@RestController
@RequestMapping(value="/")
public class BookShelfController {
	
	@Autowired 
	private BookRepository bookRepository;
	@Autowired 
	private BookService bookService;
	
	@GetMapping(path= "/returnAllBooks")
	public ResponseEntity<List<BookEntity>>returnAllBooks(){
		List<BookEntity> books = bookRepository.findAll();
		if(books.isEmpty())
			return new ResponseEntity<List<BookEntity>>(HttpStatus.NO_CONTENT);
		 return new ResponseEntity<List<BookEntity>>(books, HttpStatus.OK);
	}
	
	@PostMapping(path= "/saveBook", consumes = "application/json")
	public ResponseEntity<Void> saveBook(
			@RequestBody BookEntity book) {
		 bookService.saveBook(book.getTitle(), book.getAuthor(), book.getGenre(), book.getYear(), book.getEmail());
		 return new ResponseEntity<>( HttpStatus.OK);
	}
	
	@PostMapping(path= "/deleteBook", consumes = "application/json")
	public void deleteBook(@RequestBody BookEntity book) {
		bookService.deleteBook(book.getId());
	}
	

}
