package it.fabri.code.readshare.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.fabri.code.readshare.entities.BookEntity;
import it.fabri.code.readshare.repositories.BookRepository;

@Service
public class BookService {

	@Autowired
	BookRepository bookRepository;
	
	public void saveBook(String title, String author, String genre, String year, String email) {
		BookEntity book = new BookEntity();
		book.setTitle(title);
		book.setAuthor(author);
		book.setGenre(genre);
		book.setYear(year);
		book.setEmail(email);
		bookRepository.save(book);
	}
	public void deleteBook(long id) {
		bookRepository.deleteById(id);
	}
	
}
