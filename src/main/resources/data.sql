DROP TABLE IF EXISTS book_Repository;
 
CREATE TABLE book_Repository (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  title VARCHAR(250) NOT NULL,
  author VARCHAR(250) NOT NULL,
  genre VARCHAR(250) NOT NULL,
  year VARCHAR(250) NOT NULL,
  email VARCHAR(250) NOT NULL
);
 
INSERT INTO book_Repository (title , author, genre, year, email) VALUES
  ('Harry Potter 1', 'J.K Rawling', 'Fantasy','2008', 'fabriziogiannitrapani@gmail.com'),
  ('Harry Potter 2', 'J.K Rawling', 'Fantasy','2009', 'fabriziogiannitrapani@gmail.com'),
  ('The Canterbury Tales', 'J. Chaucer', 'Novels', '1400', 'fabriziogiannitrapani@gmail.com'),
  ('Divina comoedia', 'D. Alighieri', 'Poem','1320', 'fabriziogiannitrapani@gmail.com'),
  ('Harry Potter 3', 'J.K Rawling', 'Fantasy','2010', 'fabriziogiannitrapani@gmail.com');
  

